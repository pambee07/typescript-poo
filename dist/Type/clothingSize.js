"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.clothingSize = void 0;
var clothingSize;
(function (clothingSize) {
    clothingSize["S"] = "S";
    clothingSize["M"] = "M";
    clothingSize["L"] = "L";
    clothingSize["XL"] = "XL";
    clothingSize["XXL"] = "XXL";
})(clothingSize || (exports.clothingSize = clothingSize = {}));
