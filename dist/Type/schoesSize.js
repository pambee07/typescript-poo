"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.shoesSize = void 0;
var shoesSize;
(function (shoesSize) {
    shoesSize["EU_36"] = "EU_36";
    shoesSize["EU_37"] = "EU_37";
    shoesSize["EU_38"] = "EU_38";
    shoesSize["EU_39"] = "EU_39";
    shoesSize["EU_40"] = "EU_40";
    shoesSize["EU_41"] = "EU_41";
    shoesSize["EU_42"] = "EU_42";
    shoesSize["EU_43"] = "EU_43";
    shoesSize["EU_44"] = "EU_44";
    shoesSize["EU_45"] = "EU_45";
    shoesSize["EU_46"] = "EU_46";
})(shoesSize || (exports.shoesSize = shoesSize = {}));
