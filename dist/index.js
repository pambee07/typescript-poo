"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const clothing_1 = __importDefault(require("./Classe/clothing"));
const shoes_1 = __importDefault(require("./Classe/shoes"));
const customer_1 = __importDefault(require("./Classe/customer"));
const product_1 = __importDefault(require("./Classe/product"));
const clothingSize_1 = require("./Type/clothingSize");
const schoesSize_1 = require("./Type/schoesSize");
const order_1 = __importDefault(require("./Classe/order"));
// test de la classe Customer
let customer = new customer_1.default(1, "John", "tt@tt.tt");
console.log(customer.displayAddress());
customer.setAddress({
    street: "rue de la paix",
    city: "Paris",
    postalCode: "75000",
    country: "France"
});
console.log(customer.displayAddress());
// test de la classe Product
//let product = new Product(1, "pq", 2, 10);
//console.log(product.displayDetails());
// test de la classe Clothing
const product = new product_1.default(1, "Table", 10, 100, { length: 100, width: 50, height: 75 });
//console.log(product.displayDetails());
// test de la classe Clothing
const clothing1 = new clothing_1.default(2, "T-shirt", 0.2, 15, { length: 25, width: 30, height: 2 }, clothingSize_1.clothingSize.M);
//console.log(clothing1.displayDetails());
// test de la classe Shoes
const shoes1 = new shoes_1.default(3, "Sneakers", 0.8, 50, { length: 30, width: 10, height: 10 }, schoesSize_1.shoesSize.EU_42);
//console.log(shoes1.displayDetails());
const now = new Date;
let ordertest = new order_1.default(1, customer, [product, clothing1, shoes1], now);
console.log(ordertest.displayOrder());
