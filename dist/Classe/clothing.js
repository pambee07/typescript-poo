"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const product_js_1 = __importDefault(require("./product.js"));
class Clothing extends product_js_1.default {
    constructor(productId, name, weight, price, dimensions, size) {
        super(productId, name, weight, price, dimensions);
        this.size = size;
    }
    displayDetails() {
        return `Product ID: ${this.productId}, Name: ${this.name}, Weight: ${this.weight}, Price: ${this.price} €, Dimensions: ${this.dimensions.length}x${this.dimensions.width}x${this.dimensions.height}, Size: ${this.size}`;
    }
}
exports.default = Clothing;
