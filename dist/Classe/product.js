"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Product {
    // Modifier le constructeur pour prendre en compte la propriété dimensions
    constructor(productId, name, weight, price, dimensions) {
        this.productId = productId;
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.dimensions = dimensions;
    }
    displayDetails() {
        return `Product ID: ${this.productId}, Name: ${this.name}, Weight: ${this.weight}, Price: ${this.price} €, Dimensions: ${this.dimensions.length}x${this.dimensions.width}x${this.dimensions.height}`;
    }
}
exports.default = Product;
