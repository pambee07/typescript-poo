"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Customer {
    constructor(customerId, name, email) {
        this.customerId = customerId;
        this.name = name;
        this.email = email;
    }
    // Ajout de la méthode setAddress
    setAddress(address) {
        this.address = address;
    }
    // Ajout de la méthode displayInfo
    displayInfo() {
        return `Customer ID: ${this.customerId}, Name: ${this.name}, Email: ${this.email}, Address: ${this.displayAddress()}`;
    }
    //   Ajout de la méthode displayAddress
    displayAddress() {
        if (this.address) {
            return `Address: ${this.address.street}, ${this.address.city}, ${this.address.postalCode}, ${this.address.country}`;
        }
        else {
            return "No address found";
        }
        ;
    }
}
exports.default = Customer;
