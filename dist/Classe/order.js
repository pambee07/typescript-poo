"use strict";
// import { Dimensions } from "../Type/dimension";
Object.defineProperty(exports, "__esModule", { value: true });
class Order {
    constructor(orderId, customer, productList, orderDate) {
        this.orderId = orderId;
        this.customer = customer;
        this.productList = productList;
        this.orderDate = orderDate;
    }
    // Ajouter un produit à la commande
    addProduct(product) {
        this.productList.push(product);
    }
    //Retire un produit de la commande.
    removeProduct(productId) {
        this.productList.splice(productId);
    }
    //Calcule le poids total de la commande.
    calculateWeight() {
        let sum = 0;
        return this.productList.forEach(element => sum += element.weight);
    }
    //Calcule le prix total de la commande.
    calculateTotal() {
        let sum = 0;
        return this.productList.forEach(element => sum += element.price);
    }
    displayOrder() {
        return `Order : ${this.orderId} , ${this.customer.displayInfo()}, ${this.productList.forEach(element => element.displayDetails())}, Total : ${this.calculateTotal()} €`;
    }
}
exports.default = Order;
