import Clothing from "./Classe/clothing";
import Shoes from "./Classe/shoes";
import Customer from "./Classe/customer";
import Product from "./Classe/product";
import { clothingSize } from "./Type/clothingSize";
import { shoesSize } from "./Type/schoesSize";
import Order from "./Classe/order";

// test de la classe Customer
let customer = new Customer(1, "John", "tt@tt.tt");
console.log(customer.displayAddress());
customer.setAddress({
    street: "rue de la paix",
    city: "Paris",
    postalCode: "75000",
    country: "France"
  });
console.log(customer.displayAddress());


// test de la classe Product
//let product = new Product(1, "pq", 2, 10);
//console.log(product.displayDetails());

// test de la classe Clothing
const product = new Product(1, "Table", 10, 100, {length: 100, width: 50, height: 75});
//console.log(product.displayDetails());

// test de la classe Clothing
const clothing1 = new Clothing(2, "T-shirt", 0.2, 15, {length: 25, width: 30, height: 2}, clothingSize.M);
//console.log(clothing1.displayDetails());

// test de la classe Shoes
const shoes1 = new Shoes(3, "Sneakers", 0.8, 50, {length: 30, width: 10, height: 10}, shoesSize.EU_42);
//console.log(shoes1.displayDetails());

const now = new Date;
let ordertest= new Order ( 1 , customer, [product, clothing1, shoes1], now )
console.log(ordertest.displayOrder());
