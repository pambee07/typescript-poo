import { Address } from "../Type/address";

export default class Customer {
    customerId: number;
    name: string;
    email: string;
    address: Address|undefined;
    

    constructor(customerId: number, name: string, email: string) {
      this.customerId = customerId;
      this.name = name;
      this.email = email;
    } 

    // Ajout de la méthode setAddress
    setAddress(address: Address) {
    this.address = address;
    }

    // Ajout de la méthode displayInfo
    displayInfo() {
        return `Customer ID: ${this.customerId}, Name: ${this.name}, Email: ${this.email}, Address: ${this.displayAddress()}`;
  }
    //   Ajout de la méthode displayAddress
    displayAddress() {
        if (this.address) {
            return `Address: ${this.address.street}, ${this.address.city}, ${this.address.postalCode}, ${this.address.country}`;
        }
        else {return "No address found"};
    } 
}