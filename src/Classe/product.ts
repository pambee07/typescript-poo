import { Dimensions } from "../Type/dimension";

export default class Product {
    productId: number;
    name: string;
    weight: number;
    price: number;
    // Ajout de la propriété dimensions de type Dimensions
    dimensions: Dimensions;
   
    // Modifier le constructeur pour prendre en compte la propriété dimensions
    constructor(productId: number, name: string, weight: number, price: number, dimensions: Dimensions) {
      this.productId = productId;
      this.name = name;
      this.weight = weight;
      this.price = price;
      this.dimensions = dimensions;
    }

    displayDetails() {
        return `Product ID: ${this.productId}, Name: ${this.name}, Weight: ${this.weight}, Price: ${this.price} €, Dimensions: ${this.dimensions.length}x${this.dimensions.width}x${this.dimensions.height}`;
  }
}