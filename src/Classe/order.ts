// import { Dimensions } from "../Type/dimension";

import Customer from "./customer";
import Product from "./product";

export default class Order {
    orderId: number;
    customer: Customer;
    productList: Product[];
    orderDate: Date;

    
    constructor(orderId: number,customer: Customer,productList: Product[],orderDate: Date) {
      this.orderId = orderId;
      this.customer = customer;
      this.productList = productList;
      this.orderDate = orderDate;
    }

    // Ajouter un produit à la commande
    addProduct(product: Product) {
        this.productList.push(product);
    }

    //Retire un produit de la commande.
    removeProduct(productId: number){
        this.productList.splice(productId);
    }

    //Calcule le poids total de la commande.
    calculateWeight(){
        let sum: number = 0;
        return this.productList.forEach( element => sum += element.weight)
    }

    //Calcule le prix total de la commande.
    calculateTotal(){
        let sum: number = 0;
        return this.productList.forEach( element => sum += element.price)
    }

    displayOrder() {
        return `Order : ${this.orderId} , ${this.customer.displayInfo()}, ${this.productList.forEach( element => element.displayDetails())}, Total : ${this.calculateTotal()} €`;
  }
}