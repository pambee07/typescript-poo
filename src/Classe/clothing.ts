import { clothingSize } from '../Type/clothingSize.js';
import Product from './product.js';
import { Dimensions } from '../Type/dimension.js';

class Clothing extends Product {
    size: clothingSize;
 constructor (
    productId: number, 
    name: string, 
    weight: number, 
    price: number, 
    dimensions: Dimensions, 
    size: clothingSize
    ) {
    super(productId, name, weight, price, dimensions);
    this.size = size;
    }

    displayDetails() {
        return `Product ID: ${this.productId}, Name: ${this.name}, Weight: ${this.weight}, Price: ${this.price} €, Dimensions: ${this.dimensions.length}x${this.dimensions.width}x${this.dimensions.height}, Size: ${this.size}`;
    }
}

    export default Clothing;
