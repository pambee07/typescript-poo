// Ajout du type Dimensions
export type Dimensions = {
    length: number;
    width: number;
    height: number;
}